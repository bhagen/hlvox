CHANGES
=======

* Add basic docstring
* Maybe fix version detection

0.5
---

* Use separate password for test.pypi
* Update build setup
* Add speed modifier (needs some work)
* Word modifiers

0.4.3
-----

* Don't specify full version
* Finalize 3.11 update
* Upgrade CI to Python 3.11
* Update to Python 3.11

0.4.2
-----

* Switch to binary psycopg2

0.4.1
-----

* Add missing dep

0.4.0
-----

* Pick up a real database

0.3.12
------

* Output requirements.txt during build

0.3.11
------

* Update readme
* Ignore tests in security scan
* Missed a "pipenv run"
* Exclude some files from security scan
* Pre-commit in CI
* Configure SAST in \`.gitlab-ci.yml\`, creating this file if it does not already exist

0.3.10
------

* Add and apply pre-commit hooks

0.3.9
-----

* Maybe fix multi punctuation

0.3.8
-----

* Cleanup stored sentences

0.3.7
-----

* Minimum voice definition in sentence

0.3.6
-----

* Voice categories
* Don't save single-word sentences

0.3.5
-----

* Resolve "Multi random voice tries to use lower case voice names"

0.3.4
-----

* Add support for multi-voice mode

0.3.3
-----

* Fix differing frame rate artifacting
* Add some ignores for convenience

0.3.2
-----

* Fix duplicate DB entries

0.3.1
-----

* Fix some broken tests
* Allow for dry runs of sentence generation

0.3.0
-----

* Export audio data directly rather than to wav

0.2.2
-----

* Handle filenames with capitals

0.2.1
-----

* Use dataclass for sentence info return
* Tweak imports to get tests working in vscode

0.2.0
-----

* Add some more type hinting
* Update readme
* Remove pipfile (for now)
* Bump to 3.9
* Sort voice names
* Don't think I need this
* Apparently yield\_fixture is deprecated
* Install package before coverage
* Only tox in 3.9
* Enable pytest in CI
* Unittest -> pytest
* Add context manager
* Change position args to keywords
* Whitespace, dead code, import ordering
* Remove some dead code

0.1.9
-----

* force frame rate
* refactored sentence creation logic
* added main and args
* import ordering
* self.logger -> log

0.1.7
-----

* added dict option for getting generated sentences

0.1.6
-----

* Update .gitlab-ci.yml
* Update .gitlab-ci.yml
* removed need for punctuation files
* only deploy on master branch

0.1.5
-----

* changed to seperate folder for db
* ignore noseids
* added pylint
* build changes
* change to venv caching
* added requirements.txt
* added authors
* ignore dist and build
* updated

0.1.4
-----

* generate wheel
* fixed classifier
* added auto changelog
* added pypi readme
* changed to pbr
* version change

0.1.3
-----

* remove variable
* moved around before\_script stuff
* remove tox
* add author
* Update .gitlab-ci.yml
* Update .gitlab-ci.yml
* added package name
* egg stuff
* deploy stage
* description and versioning
* Update README.md
* remove ffmpeg
* Update README.md
* rem ffmpeg
* added ffmpeg install
* added twine and ffmpeg
* changed coverage path
* changed coverage command
* pipenv install dev
* ignore tox
* changed test command
* added tox
* Update .gitlab-ci.yml
* changed before operation to pipenv
* generated apidocs
* added one more level of back path
* not needed anymore
* cleaned up
* updated pipfile
* Update README.md
* removed unused funct
* coverage report
* added coverage
* changed apidoc url
* change to nose for tests
* changed module name and test fw
* Update README.md
* Update README.md
* Add license
* Update README.md
* Add readme template
* Delete README.rst
* remove egg info
* added back cd
* changed sphynx path
* moved again
* added documentation
* removed poetry stuff
* added documentation
* only 3.7
* flake8
* changed to count
* removed test requirement
* removed requirement
* change to requirements.txt
* install pytest
* changing structure and adding setup
* Add ci file
* Add .gitlab-ci.yml
* changed from premade punt to silence
* changed logging and added padding
* added logging and tests for overwrite issue
* fixed db sentence search formatting
* changed test
* fixed sayable sentence string return
* imports work better now
* moved stuff around again because I don't understand imports
* removed comments
* added voice get tests
* added voice get none return
* removed exception for punctuation
* changed to subdir export cleaning
* added manager tests
* changed import and added exit for testing
* configuring imports
* changed import and export behavior
* renamed
* fixed import
* renamed voxvoice to voice
* moved manager to voice
* type hinting
* additional logging
* docstrings and logging
* added whitespace stripping
* added getter for generated sentences
* added generated sentence getter test
* comment cleanup
* removed db purge
* changed cleanup behavior
* changed cleanup variable
* converted process to sentence list tests
* added sentence list function
* added tilename from sentence testing
* added tinydb
* added exiting of class
* added db cleaning
* switched to tinydb
* sorted output check
* words output is sorted now
* added repeated punc
* changed punctuation behavior
* get\_words now returns list
* fixed wrong array sent to filename creation
* added duplicate word generation testing
* added wave package
* added wave package
* spacing
* creates real wav files now
* added option to only clean exports
* added duplicate generation test
* added sayable sentence test
* refactored to split up process\_sentence
* added lots of new tests
* punctuation tests
* added function for creating sentence files
* generated filenames
* changed to private again
* changed to getter
* basic word dict testing
* export cleaning
* refac variables and added export cleaning
* alarm info checking
* moved info cleaning into main function
* fixed duplicate test file declaration
* I broke the tests by merging :(
* added info testing
* changed priv to pub variable
* added file format checker
* added autopep8
* basic test files added
* exception error messages
* test temp files
* ffmpeg
* pyc
* test framework
* changed behavior when can't say sentence
* vs code dev stuff
* added .vscode
* removed egg info from repo
* added words retrieve
* added pylint
* added .venv
* added gitignore
* changed comparison
* moved to poetry
* added init file
* added back pipfile
* Updated from other module
* Moved some packages to dev packages
* Added vox info for holding alarm names
* Allowed for folders to exist in vox voice file directory
* Fixed audio generation. Tested on wav
* added audio generation, needs path corrections
* added pydub for media concatenation
* Added processing of sentence
* added format finding
* Initial code commit
* Added piplock
* Added base requirements
* Added pipfile
